%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "fields": {
    "summary": {
      "required": true,
      "schema": {
        "type": "string",
        "system": "summary"
      },
      "name": "Summary",
      "key": "summary",
      "operations": [
        "set"
      ]
    },
    "issuetype": {
      "required": true,
      "schema": {
        "type": "issuetype",
        "system": "issuetype"
      },
      "name": "Issue Type",
      "key": "issuetype",
      "operations": [],
      "allowedValues": [
        {
          "self": "https://karthikc.atlassian.net/rest/api/3/issuetype/10005",
          "id": "10005",
          "description": "Stories track functionality or features expressed as user goals.",
          "iconUrl": "https://karthikc.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10315?size=medium",
          "name": "Story",
          "subtask": false,
          "avatarId": 10315,
          "entityId": "f5546141-7654-4b8a-a8f3-e4013a1f8106",
          "hierarchyLevel": 0
        },
        {
          "self": "https://karthikc.atlassian.net/rest/api/3/issuetype/10006",
          "id": "10006",
          "description": "Tasks track small, distinct pieces of work.",
          "iconUrl": "https://karthikc.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10318?size=medium",
          "name": "Task",
          "subtask": false,
          "avatarId": 10318,
          "entityId": "6cde14fa-304c-4904-8bf1-74515f247139",
          "hierarchyLevel": 0
        },
        {
          "self": "https://karthikc.atlassian.net/rest/api/3/issuetype/10007",
          "id": "10007",
          "description": "Bugs track problems or errors.",
          "iconUrl": "https://karthikc.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10303?size=medium",
          "name": "Bug",
          "subtask": false,
          "avatarId": 10303,
          "entityId": "4d099c99-ab82-442c-b659-ccbf97efcaa7",
          "hierarchyLevel": 0
        }
      ]
    },
    "description": {
      "required": false,
      "schema": {
        "type": "string",
        "system": "description"
      },
      "name": "Description",
      "key": "description",
      "operations": [
        "set"
      ]
    },
    "customfield_10020": {
      "required": false,
      "schema": {
        "type": "array",
        "items": "json",
        "custom": "com.pyxis.greenhopper.jira:gh-sprint",
        "customId": 10020
      },
      "name": "Sprint",
      "key": "customfield_10020",
      "operations": [
        "set"
      ]
    },
    "reporter": {
      "required": true,
      "schema": {
        "type": "user",
        "system": "reporter"
      },
      "name": "Reporter",
      "key": "reporter",
      "autoCompleteUrl": "https://karthikc.atlassian.net/rest/api/3/user/search?query=",
      "operations": [
        "set"
      ]
    },
    "customfield_10021": {
      "required": false,
      "schema": {
        "type": "array",
        "items": "option",
        "custom": "com.atlassian.jira.plugin.system.customfieldtypes:multicheckboxes",
        "customId": 10021
      },
      "name": "Flagged",
      "key": "customfield_10021",
      "operations": [
        "add",
        "set",
        "remove"
      ],
      "allowedValues": [
        {
          "self": "https://karthikc.atlassian.net/rest/api/3/customFieldOption/10019",
          "value": "Impediment",
          "id": "10019"
        }
      ]
    },
    "customfield_10000": {
      "required": false,
      "schema": {
        "type": "any",
        "custom": "com.atlassian.jira.plugins.jira-development-integration-plugin:devsummarycf",
        "customId": 10000
      },
      "name": "Development",
      "key": "customfield_10000",
      "operations": [
        "set"
      ]
    },
    "customfield_10034": {
      "required": false,
      "schema": {
        "type": "string",
        "custom": "com.atlassian.jira.plugin.system.customfieldtypes:textfield",
        "customId": 10034
      },
      "name": "Reference-Id",
      "key": "customfield_10034",
      "operations": [
        "set"
      ]
    },
    "labels": {
      "required": false,
      "schema": {
        "type": "array",
        "items": "string",
        "system": "labels"
      },
      "name": "Labels",
      "key": "labels",
      "autoCompleteUrl": "https://karthikc.atlassian.net/rest/api/1.0/labels/10003/suggest?query=",
      "operations": [
        "add",
        "set",
        "remove"
      ]
    },
    "customfield_10016": {
      "required": false,
      "schema": {
        "type": "number",
        "custom": "com.pyxis.greenhopper.jira:jsw-story-points",
        "customId": 10016
      },
      "name": "Story point estimate",
      "key": "customfield_10016",
      "operations": [
        "set"
      ]
    },
    "environment": {
      "required": false,
      "schema": {
        "type": "string",
        "system": "environment"
      },
      "name": "Environment",
      "key": "environment",
      "operations": [
        "set"
      ]
    },
    "customfield_10019": {
      "required": false,
      "schema": {
        "type": "any",
        "custom": "com.pyxis.greenhopper.jira:gh-lexo-rank",
        "customId": 10019
      },
      "name": "Rank",
      "key": "customfield_10019",
      "operations": [
        "set"
      ]
    },
    "attachment": {
      "required": false,
      "schema": {
        "type": "array",
        "items": "attachment",
        "system": "attachment"
      },
      "name": "Attachment",
      "key": "attachment",
      "operations": [
        "set"
      ]
    },
    "issuelinks": {
      "required": false,
      "schema": {
        "type": "array",
        "items": "issuelinks",
        "system": "issuelinks"
      },
      "name": "Linked Issues",
      "key": "issuelinks",
      "autoCompleteUrl": "https://karthikc.atlassian.net/rest/api/3/issue/picker?currentProjectId=&showSubTaskParent=true&showSubTasks=true&currentIssueKey=BSJS-1&query=",
      "operations": [
        "add"
      ]
    },
    "comment": {
      "required": false,
      "schema": {
        "type": "comments-page",
        "system": "comment"
      },
      "name": "Comment",
      "key": "comment",
      "operations": [
        "add",
        "edit",
        "remove"
      ]
    },
    "assignee": {
      "required": false,
      "schema": {
        "type": "user",
        "system": "assignee"
      },
      "name": "Assignee",
      "key": "assignee",
      "autoCompleteUrl": "https://karthikc.atlassian.net/rest/api/3/user/assignable/search?issueKey=BSJS-1&query=",
      "operations": [
        "set"
      ]
    }
  }
})