%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "id": null,
  "items": [
    {
      "exception": null,
      "message": null,
      "payload": {
        "success": true,
        "id": "5005g00000KLxt0AAD",
        "errors": []
      },
      "id": "5005g00000KLxt0AAD",
      "successful": true,
      "statusCode": null
    }
  ],
  "successful": true
})