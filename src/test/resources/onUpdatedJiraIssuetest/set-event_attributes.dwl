{
  "headers": {
    "server": "AtlassianProxy/1.19.3.1",
    "vary": "Accept-Encoding",
    "cache-control": "no-cache, no-store, no-transform",
    "content-type": "application/json;charset=UTF-8",
    "strict-transport-security": "max-age=315360000; includeSubDomains; preload",
    "date": "Sun, 06 Feb 2022 17:08:01 GMT",
    "atl-traceid": "609e602bcadcff7e",
    "x-arequestid": "96ac97f8-ab4a-480f-9ade-94e526c9bd64",
    "x-aaccountid": "61fa2e4d845d670071f30d28",
    "x-xss-protection": "1; mode=block",
    "transfer-encoding": "chunked",
    "timing-allow-origin": "*",
    "x-envoy-upstream-service-time": "222",
    "x-content-type-options": "nosniff",
    "connection": "close",
    "set-cookie": "atlassian.xsrf.token=d95a5317-7f38-4434-8a6d-0f61eb014ee2_37be1ab47f46745178deb8c6ac319fa4327483bb_lin; path=/; SameSite=None; Secure",
    "expect-ct": "report-uri=\"https://web-security-reports.services.atlassian.com/expect-ct-report/global-proxy\", enforce, max-age=86400"
  },
  "reasonPhrase": "OK",
  "statusCode": 200
}