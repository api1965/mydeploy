{
  "expand": "operations,versionedRepresentations,editmeta,changelog,customfield_10010.requestTypePractice,renderedFields",
  "id": "10013",
  "self": "https://karthikc.atlassian.net/rest/api/3/issue/10013",
  "key": "BSJS-10",
  "fields": {
    "statuscategorychangedate": "2022-02-06T22:04:46.015+0530",
    "issuetype": {
      "self": "https://karthikc.atlassian.net/rest/api/3/issuetype/10007",
      "id": "10007",
      "description": "Bugs track problems or errors.",
      "iconUrl": "https://karthikc.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10303?size=medium",
      "name": "Bug",
      "subtask": false,
      "avatarId": 10303,
      "entityId": "4d099c99-ab82-442c-b659-ccbf97efcaa7",
      "hierarchyLevel": 0
    },
    "timespent": null,
    "project": {
      "self": "https://karthikc.atlassian.net/rest/api/3/project/10001",
      "id": "10001",
      "key": "BSJS",
      "name": "bidirectional-salesforce-jira-sync",
      "projectTypeKey": "software",
      "simplified": true,
      "avatarUrls": {
        "48x48": "https://karthikc.atlassian.net/rest/api/3/universal_avatar/view/type/project/avatar/10408",
        "24x24": "https://karthikc.atlassian.net/rest/api/3/universal_avatar/view/type/project/avatar/10408?size=small",
        "16x16": "https://karthikc.atlassian.net/rest/api/3/universal_avatar/view/type/project/avatar/10408?size=xsmall",
        "32x32": "https://karthikc.atlassian.net/rest/api/3/universal_avatar/view/type/project/avatar/10408?size=medium"
      }
    },
    "customfield_10032": null,
    "customfield_10033": null,
    "fixVersions": [],
    "customfield_10034": "5005g00000KLxgUAAT",
    "aggregatetimespent": null,
    "resolution": null,
    "resolutiondate": null,
    "workratio": -1,
    "lastViewed": "2022-02-06T22:37:50.014+0530",
    "watches": {
      "self": "https://karthikc.atlassian.net/rest/api/3/issue/BSJS-10/watchers",
      "watchCount": 1,
      "isWatching": true
    },
    "created": "2022-02-06T22:04:45.759+0530",
    "customfield_10020": null,
    "customfield_10021": null,
    "customfield_10022": null,
    "priority": {
      "self": "https://karthikc.atlassian.net/rest/api/3/priority/3",
      "iconUrl": "https://karthikc.atlassian.net/images/icons/priorities/medium.svg",
      "name": "Medium",
      "id": "3"
    },
    "customfield_10023": null,
    "customfield_10024": null,
    "customfield_10025": null,
    "labels": [],
    "customfield_10016": null,
    "customfield_10017": null,
    "customfield_10018": {
      "hasEpicLinkFieldDependency": false,
      "showField": false,
      "nonEditableReason": {
        "reason": "PLUGIN_LICENSE_ERROR",
        "message": "The Parent Link is only available to Jira Premium users."
      }
    },
    "customfield_10019": "0|i0002n:",
    "timeestimate": null,
    "aggregatetimeoriginalestimate": null,
    "versions": [],
    "issuelinks": [],
    "assignee": null,
    "updated": "2022-02-06T22:37:55.886+0530",
    "status": {
      "self": "https://karthikc.atlassian.net/rest/api/3/status/10003",
      "description": "",
      "iconUrl": "https://karthikc.atlassian.net/",
      "name": "To Do",
      "id": "10003",
      "statusCategory": {
        "self": "https://karthikc.atlassian.net/rest/api/3/statuscategory/2",
        "id": 2,
        "key": "new",
        "colorName": "blue-gray",
        "name": "To Do"
      }
    },
    "components": [],
    "timeoriginalestimate": null,
    "description": {
      "version": 1,
      "type": "doc",
      "content": [
        {
          "type": "paragraph",
          "content": [
            {
              "type": "text",
              "text": "D"
            }
          ]
        }
      ]
    },
    "customfield_10010": null,
    "customfield_10014": null,
    "customfield_10015": null,
    "customfield_10005": null,
    "customfield_10006": null,
    "security": null,
    "customfield_10007": null,
    "customfield_10008": null,
    "customfield_10009": null,
    "aggregatetimeestimate": null,
    "summary": "KARTHIK c",
    "creator": {
      "self": "https://karthikc.atlassian.net/rest/api/3/user?accountId=61fa2e4d845d670071f30d28",
      "accountId": "61fa2e4d845d670071f30d28",
      "emailAddress": "karthik.c.c@apisero.com",
      "avatarUrls": {
        "48x48": "https://secure.gravatar.com/avatar/6cec693caa92bae5e7e81589094d2149?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FKC-6.png",
        "24x24": "https://secure.gravatar.com/avatar/6cec693caa92bae5e7e81589094d2149?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FKC-6.png",
        "16x16": "https://secure.gravatar.com/avatar/6cec693caa92bae5e7e81589094d2149?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FKC-6.png",
        "32x32": "https://secure.gravatar.com/avatar/6cec693caa92bae5e7e81589094d2149?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FKC-6.png"
      },
      "displayName": "Karthik C",
      "active": true,
      "timeZone": "Asia/Calcutta",
      "accountType": "atlassian"
    },
    "subtasks": [],
    "reporter": {
      "self": "https://karthikc.atlassian.net/rest/api/3/user?accountId=61fa2e4d845d670071f30d28",
      "accountId": "61fa2e4d845d670071f30d28",
      "emailAddress": "karthik.c.c@apisero.com",
      "avatarUrls": {
        "48x48": "https://secure.gravatar.com/avatar/6cec693caa92bae5e7e81589094d2149?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FKC-6.png",
        "24x24": "https://secure.gravatar.com/avatar/6cec693caa92bae5e7e81589094d2149?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FKC-6.png",
        "16x16": "https://secure.gravatar.com/avatar/6cec693caa92bae5e7e81589094d2149?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FKC-6.png",
        "32x32": "https://secure.gravatar.com/avatar/6cec693caa92bae5e7e81589094d2149?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FKC-6.png"
      },
      "displayName": "Karthik C",
      "active": true,
      "timeZone": "Asia/Calcutta",
      "accountType": "atlassian"
    },
    "customfield_10000": "{}",
    "aggregateprogress": {
      "progress": 0,
      "total": 0
    },
    "customfield_10001": null,
    "customfield_10002": null,
    "customfield_10003": null,
    "customfield_10004": null,
    "environment": null,
    "duedate": null,
    "progress": {
      "progress": 0,
      "total": 0
    },
    "votes": {
      "self": "https://karthikc.atlassian.net/rest/api/3/issue/BSJS-10/votes",
      "votes": 0,
      "hasVoted": false
    }
  }
}