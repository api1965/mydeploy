%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "expand": "projects",
  "projects": [
    {
      "self": "https://karthikc.atlassian.net/rest/api/3/project/10001",
      "id": "10001",
      "key": "BSJS",
      "name": "bidirectional-salesforce-jira-sync",
      "avatarUrls": {
        "48x48": "https://karthikc.atlassian.net/rest/api/3/universal_avatar/view/type/project/avatar/10408",
        "24x24": "https://karthikc.atlassian.net/rest/api/3/universal_avatar/view/type/project/avatar/10408?size=small",
        "16x16": "https://karthikc.atlassian.net/rest/api/3/universal_avatar/view/type/project/avatar/10408?size=xsmall",
        "32x32": "https://karthikc.atlassian.net/rest/api/3/universal_avatar/view/type/project/avatar/10408?size=medium"
      },
      "issuetypes": [
        {
          "self": "https://karthikc.atlassian.net/rest/api/3/issuetype/10005",
          "id": "10005",
          "description": "Stories track functionality or features expressed as user goals.",
          "iconUrl": "https://karthikc.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10315?size=medium",
          "name": "Story",
          "untranslatedName": "Story",
          "subtask": false,
          "scope": {
            "type": "PROJECT",
            "project": {
              "id": "10001"
            }
          }
        },
        {
          "self": "https://karthikc.atlassian.net/rest/api/3/issuetype/10006",
          "id": "10006",
          "description": "Tasks track small, distinct pieces of work.",
          "iconUrl": "https://karthikc.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10318?size=medium",
          "name": "Task",
          "untranslatedName": "Task",
          "subtask": false,
          "scope": {
            "type": "PROJECT",
            "project": {
              "id": "10001"
            }
          }
        },
        {
          "self": "https://karthikc.atlassian.net/rest/api/3/issuetype/10007",
          "id": "10007",
          "description": "Bugs track problems or errors.",
          "iconUrl": "https://karthikc.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10303?size=medium",
          "name": "Bug",
          "untranslatedName": "Bug",
          "subtask": false,
          "scope": {
            "type": "PROJECT",
            "project": {
              "id": "10001"
            }
          }
        },
        {
          "self": "https://karthikc.atlassian.net/rest/api/3/issuetype/10009",
          "id": "10009",
          "description": "Epics track collections of related bugs, stories, and tasks.",
          "iconUrl": "https://karthikc.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10307?size=medium",
          "name": "Epic",
          "untranslatedName": "Epic",
          "subtask": false,
          "scope": {
            "type": "PROJECT",
            "project": {
              "id": "10001"
            }
          }
        },
        {
          "self": "https://karthikc.atlassian.net/rest/api/3/issuetype/10010",
          "id": "10010",
          "description": "Subtasks track small pieces of work that are part of a larger task.",
          "iconUrl": "https://karthikc.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10316?size=medium",
          "name": "Subtask",
          "untranslatedName": "Subtask",
          "subtask": true,
          "scope": {
            "type": "PROJECT",
            "project": {
              "id": "10001"
            }
          }
        }
      ]
    }
  ]
})